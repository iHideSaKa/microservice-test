# **Product Microservice**

### **Installing**

To install this application, you need to go to the root folder and run the following maven command:

```
mvn package
```

if not modified the version of the application on the pom.xml file, then this command will generate the artifact to be deployed on the target server, with the name:

```
microservice-0.0.1-SNAPSHOT.jar
```

The second step is to deploy the generated artifact and installing it to the server, so it can be run.

### **Database**

Regarding the database which shall be used, currently it's using a MongoDB cluster database created for this test application, the configuration regarding these configurations are currently on the following paths:

- src/main/resources/application.properties (this is for the running application)
- src/test/resources/test.properties (this is for test purposes)

So in case of changing the database, those are the files to be modified.

### **Running the application**

Also for running the application locally, you may run the following command:

```
mvn package && java -jar target/microservice-0.0.1-SNAPSHOT.jar
```

after it finishes building, packaging it should start on [localhost:8080](localhost:8080).


### Reference

This application was based on the following project:
https://github.com/didinj/springboot-mvc-data-mongodb