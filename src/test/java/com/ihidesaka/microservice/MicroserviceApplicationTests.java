package com.ihidesaka.microservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.ihidesaka.microservice.MicroserviceApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MicroserviceApplication.class)
@TestPropertySource(locations = "classpath:test.properties")
public class MicroserviceApplicationTests
{
	@Test
	public void contextLoads() {
	}
}
