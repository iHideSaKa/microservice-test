package com.ihidesaka.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ihidesaka.microservice.MicroserviceApplicationTests;
import com.ihidesaka.microservice.controllers.ProductController;
import com.ihidesaka.microservice.models.Product;
import com.ihidesaka.microservice.models.ProductCreationRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class TestProductController extends MicroserviceApplicationTests
{
    private MockMvc myMockMvc;

	@Autowired
	private ProductController productController;
	
	@Before
	public void setUp()
	{
		this.myMockMvc = MockMvcBuilders.standaloneSetup(productController).build();
	}
 
    private String NAME = "NAME";
    private String DESCRIPTION = "DESCRIPTION";
    private String CODE = "CODE";
    private String MANUFACTURER = "MANUFACTER";
    private String INVALID_MANUFACTURER = new Faker().lorem().characters(90);

    private static String PRODUCT_CREATION_FAILURE_ERROR_MESSAGE = "Request processing failed; nested exception is javax.validation.ConstraintViolationException: manufacturer: size must be between 0 and 80";

    private String PRODUCT_CREATION_REDIRECTION = "redirect:/show/";

    /**
     * GIVEN a {@link Product} with valid parameters
     * WHEN using the {@link Product} manual creation page
     *  and clicked on the save button
     * THEN the {@link Product} should be created with success
     *  and it should redirect to the {@link Product} details page of the created {@link Product}
     */
	@Test
    public void shouldCreateProductManually() throws Exception 
    {
		MvcResult result = myMockMvc.perform(MockMvcRequestBuilders.post("/save-product")
                    .param("pName", NAME)
                    .param("pDescription", DESCRIPTION)
                    .param("pCode", CODE)
                    .param("pManufacturer", MANUFACTURER))
                .andReturn();

        String productCreationReturn = result.getModelAndView().getViewName();

        int lastIndex = productCreationReturn.lastIndexOf("/");
        assertNotNull(lastIndex);
        lastIndex++;

        String productRedirect = productCreationReturn.substring(0, lastIndex);
        String productId = productCreationReturn.substring(lastIndex);    

        assertEquals(PRODUCT_CREATION_REDIRECTION, productRedirect);    
        assertNotNull(productId);
    }

    /**
     * GIVEN a {@link Product} with invalid parameters
     * WHEN using the {@link Product} manual creation page
     *  and clicked on the save button
     * THEN it should throw an exception
     */
	@Test
    public void shouldNotCreateProductManualWithInvalidParameter() throws Exception
    {
        try
        {
            myMockMvc.perform(MockMvcRequestBuilders.post("/save-product")
                    .param("pName", NAME)
                    .param("pDescription", DESCRIPTION)
                    .param("pCode", CODE)
                    .param("pManufacturer", INVALID_MANUFACTURER));
        }
        catch (Exception e)
        {
            assertEquals(PRODUCT_CREATION_FAILURE_ERROR_MESSAGE, e.getMessage());
        }
    }
    
    /**
     * GIVEN a database without any {@link Product}
     * WHEN a POST request is executed to return all existing {@link Product}
     * THEN it should return an empty list in json format
     */
	@Test
    public void shouldReturnEmptyListWithEmptyDatabase() throws Exception 
    {
		cleanDatabase();

		MvcResult result = myMockMvc.perform(MockMvcRequestBuilders.get("/getProducts"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();

        assertNotNull(responseContent);
        assertEquals(responseContent, "[]");
    }

    /**
     * GIVEN a database with two {@link Product}
     * WHEN a POST request is executed to return all existing {@link Product}
     * THEN it should return a list with the two {@link Product} in json format
     */
	@Test
    public void shouldReturnAllProducts() throws Exception 
    {
        cleanDatabase();

        populateDatabase("2");

		MvcResult result = myMockMvc.perform(MockMvcRequestBuilders.get("/getProducts"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();

        assertNotNull(responseContent);
        assertNotEquals(responseContent, "[]");
    }

    private void cleanDatabase() throws Exception 
    {
		myMockMvc.perform(MockMvcRequestBuilders.post("/delete-all"))
                 .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    private void populateDatabase(String pQuantity) throws Exception 
    {
        ProductCreationRequest productCreationRequest = new ProductCreationRequest(1);
        myMockMvc.perform(MockMvcRequestBuilders.post("/createProducts")
                    .content(asJsonString(productCreationRequest))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }  
}
