$(document).ready(function()
{
    function getLink(data, type, dataToSet) {
        return data = '<a href="/show/' + data.id + '">' + data.name + '</a>';
    }

    var table = $('#product-table').DataTable(
    {
        "sAjaxSource": "/getProducts",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        "aoColumns": [
            {
                "mData": getLink
            },
            {
                "mData": "description"
            },
            {
                "mData": "code"
            },
            {
                "mData": "manufacturer"
            }
        ]
    });
} );