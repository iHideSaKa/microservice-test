package com.ihidesaka.microservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.ihidesaka.microservice.builders.ProductBuilder;
import com.ihidesaka.microservice.models.Product;
import com.ihidesaka.microservice.models.ProductCreationRequest;
import com.ihidesaka.microservice.repositories.ProductRepository;

/**
 * Represents the product Controller.
 */
@Controller
public class ProductController
{

    @Autowired
    ProductRepository myProductRepository;

    /**
     * Redirects to {@link Product} listing page.
     * 
     * @param pModel the model
     * @return the {@link Product} listing page
     */
    @RequestMapping("/product")
    public String product(Model pModel)
    {
        return "product";
    }

    /**
     * Redirects to {@link Product} manual creation page.
     * 
     * @param pModel the model
     * @return the {@link Product} manual creation page
     */
    @RequestMapping("/create-product")
    public String createProduct(Model pModel)
    {
        return "create-product";
    }

    /**
     * Redirects to {@link Product} automatic page.
     * 
     * @param pModel the model
     * @return the {@link Product} automatic page
     */
    @RequestMapping("/create-products")
    public String createProducts(Model pModel)
    {
        return "create-products";
    }

    /**
     * Redirects to {@link Product} details page for the informed {@link Product} id.
     * 
     * @param pId the id
     * @param pModel the model
     * @return the {@link Product} details page for the informed {@link Product} id
     * @throws Exception
     */
    @RequestMapping("/show/{pId}")
    public String show(@PathVariable String pId, Model pModel) throws Exception
    {
        Product product = myProductRepository
                .findById(pId)
                .orElse(null);

        if (product == null)
        {
            throw new Exception("Could not load product, it was not found...");
        }
        
        pModel.addAttribute("product", product);

        return "show";
    }

    /**
     * Redirects to {@link Product} edit page for the informed {@link Product} id.
     * 
     * @param pId the id
     * @param pModel the model
     * @return the {@link Product} edit page for the informed {@link Product} id
     * @throws Exception
     */
    @RequestMapping("/edit/{pId}")
    public String edit(@PathVariable String pId, Model pModel) throws Exception
    {
        Product product = myProductRepository
                .findById(pId)
                .orElse(null);

        if (product == null)
        {
            throw new Exception("Could not load product, it was not found...");
        }

        pModel.addAttribute("product", product);

        return "edit";
    }

    /**
     * Redirects to database data clean page
     * 
     * @param pModel the model
     * @return the to database data clean page
     */
    @RequestMapping("/clean-database")
    public String cleanDatabase(Model pModel)
    {
        return "clean-database";
    }

    /**
     * Updates the {@link Product} with the informed parameters
     * and redirects to the {@link Product} details page for the updated {@link Product}.
     * 
     * @param pId the id
     * @param pName the name
     * @param pDescription the description
     * @param pCode the code
     * @param pManufacturer the manufacturer
     * @return the {@link Product} details page for the updated {@link Product}
     * @throws Exception
     */
    @RequestMapping("/update")
    public String update(@RequestParam String pId, @RequestParam String pName, @RequestParam String pDescription,
                                @RequestParam String pCode, @RequestParam String pManufacturer) throws Exception
    {
        Product product = myProductRepository
                .findById(pId)
                .orElse(null);

        if (product == null)
        {
            throw new Exception("Could not update product, it was not found...");
        }

        product.setName(pName);
        product.setDescription(pDescription);
        product.setCode(pCode);
        product.setManufacturer(pManufacturer);

        try
        {
            myProductRepository.save(product);
        }
        catch (Exception e)
        {
            throw new Exception("Problem occured while trying to update product.<br>" + e.getMessage());
        }

        return "redirect:/show/" + product.getId();
    }

    /**
     * Deletes the {@link Product}.
     * 
     * @param pId the id
     * @return the {@link Product} listing page
     * @throws Exception
     */
    @RequestMapping("/delete")
    public String delete(@RequestParam String pId) throws Exception
    {
        Product product = myProductRepository
                .findById(pId)
                .orElse(null);

        if (product == null)
        {
            throw new Exception("Cannot update, it was not found...");
        }

        try
        {
            myProductRepository.delete(product);
        } catch (Exception e) {
            throw new Exception("Problem occured while trying to delete product.<br>" + e.getMessage());
        }

        return "redirect:/product";
    }

    /**
     * Deletes all existing {@link Product}.
     * 
     * @return the {@link Product} listing page
     * @throws Exception
     */
    @RequestMapping("/delete-all")
    public String deleteAll() throws Exception
    {
        try
        {
            myProductRepository.deleteAll();
        } catch (Exception e) {
            throw new Exception("Problem occured while trying to delete product.<br>" + e.getMessage());
        }

        return "redirect:/product";
    }

    /**
     * Creates a {@link Product} bases on the parameters informed.
     * 
     * @param pName the name
     * @param pDescription the description
     * @param pCode the code
     * @param pManufacturer the manufacturer
     * @return the {@link Product} details page of the created {@link Product}
     */
    @RequestMapping("/save-product")
    public String saveProduct(@RequestParam String pName, @RequestParam String pDescription, @RequestParam String pCode, @RequestParam String pManufacturer)
    {
        Product product = new Product(pName, pDescription, pCode, pManufacturer);
        myProductRepository.save(product);

        return "redirect:/show/" + product.getId();
    }

    /**
     * Creates a number of {@link Product} with automatically generated data.
     * 
     * @param pQuantity the number of {@link Product} to be created
     * @return the {@link Product} listing page
     * @throws Exception
     */
    @RequestMapping("/save-products")
    public String saveProducts(@RequestParam Integer pQuantity) throws Exception
    {
        try
        {
            List<Product> products = new ProductBuilder().generateProducts(pQuantity);
            myProductRepository.saveAll(products);
        }
        catch (Exception e)
        {
            throw new Exception("Problem occured while trying to save products.<br>" + e.getMessage());
        }

        return "redirect:/product";
    }
    /**
     * Return a list of all existing {@link Product} in json format.
     * 
     * @return a list of all existing {@link Product} in json format
     */
    @RequestMapping(value = "/getProducts", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProducts()
    {
        return new ResponseEntity<List<Product>>((List<Product>) myProductRepository.findAll(), HttpStatus.OK);
    }

    /**
     * Creates a number of {@link Product} with automatically generated data.
     * The number of {@link Product} to be created must be informed on the following json format:
     *     {
     *         quantity: <int>
     *     }
     * 
     * @param pProductCreationRequest the number of {@link Product} to be created
     * @return a list of all existing {@link Product} in json format
     * @throws Exception
     */
    @RequestMapping(value = "/createProducts", method = RequestMethod.POST)
    public ResponseEntity<List<Product>> createProducts(@RequestBody ProductCreationRequest pProductCreationRequest) throws Exception
    {
        List<Product> products = new ProductBuilder().generateProducts(pProductCreationRequest.getQuantity());

        return new ResponseEntity<List<Product>>((List<Product>) myProductRepository.saveAll(products), HttpStatus.OK);
    }
}