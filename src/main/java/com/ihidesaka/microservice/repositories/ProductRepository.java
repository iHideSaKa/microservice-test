package com.ihidesaka.microservice.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.ihidesaka.microservice.models.Product;

public interface ProductRepository extends CrudRepository<Product, String>
{
    @Override
    Optional<Product> findById(String id);

    @Override
    void delete(Product deleted);
}
