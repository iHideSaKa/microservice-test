package com.ihidesaka.microservice.models;

public class ProductCreationRequest
{
    private int myQuantity;

    public ProductCreationRequest(int pQuantity)
    {
        this.myQuantity = pQuantity;
    }

    public ProductCreationRequest() {};

    public int getQuantity()
    {
        return this.myQuantity;
    }

    public void setQuantity(int pQuantity)
    {
        this.myQuantity = pQuantity;
    }
}
