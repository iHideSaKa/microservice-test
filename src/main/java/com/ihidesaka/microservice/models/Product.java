package com.ihidesaka.microservice.models;

import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ihidesaka.microservice.builders.ProductBuilder;

/**
 * Represents the product object.
 */
@Document(collection = "products")
public class Product
{
    @Id
    String myId;

    String myName;
    String myDescription;
    String myCode;
    String myManufacturer;

    public Product() {}

    /**
     * Constructor for this class.
     * 
     * @param pName the name
     * @param pDescription the description
     * @param pCode the code
     * @param pManufacturer the manufacturer
     */
    public Product(String pName, String pDescription, String pCode, String pManufacturer)
    {
        this.myName = pName;
        this.myDescription = pDescription;
        this.myCode = pCode;
        this.myManufacturer = pManufacturer;
    }

    /**
     * Constructor for this class.
     *
     * @param pProductBuilder an instance of {@link ProductBuilder}
     */
    public Product(ProductBuilder pProductBuilder)
    {
        this.myName = pProductBuilder.getName();
        this.myDescription = pProductBuilder.getDescription();
        this.myCode = pProductBuilder.getCode();
        this.myManufacturer = pProductBuilder.getManufacturer();
    }

    /**
     * Returns the id.
     *
     * @return the id
     */
    public String getId()
    {
        return myId;
    }

    /**
     * Sets the id.
     *
     * @param pId the id
     */
    public void setId(String pId)
    {
        this.myId = pId;
    }

    /**
     * Returns the name.
     *
     * @return the name
     */
    @Size(min = 0, max = 255)
    public String getName()
    {
        return myName;
    }

    /**
     * Sets the name.
     *
     * @param pName the name
     */
    public void setName(String pName)
    {
        this.myName = pName;
    }

    /**
     * Returns the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return myDescription;
    }

    /**
     * Sets the description.
     *
     * @param pDescription the description
     */
    public void setDescription(String pDescription)
    {
        this.myDescription = pDescription;
    }

    /**
     * Returns the code.
     *
     * @return the code
     */
    @Size(min = 0, max = 80)
    public String getCode()
    {
        return myCode;
    }

    /**
     * Sets the code.
     *
     * @param pCode the code
     */
    public void setCode(String pCode)
    {
        this.myCode = pCode;
    }

    /**
     * Returns the manufacturer.
     *
     * @return the manufacturer
     */
    @Size(min = 0, max = 80)
    public String getManufacturer()
    {
        return myManufacturer;
    }

    /**
     * Sets the manufacturer.
     *
     * @param pManufacturer the manufacturer
     */
    public void setManufacturer(String pManufacturer)
    {
        this.myManufacturer = pManufacturer;
    }
}
