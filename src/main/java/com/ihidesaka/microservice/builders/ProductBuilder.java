package com.ihidesaka.microservice.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.ihidesaka.microservice.models.Product;

/**
 * Represents the product object.
 */
public class ProductBuilder
{
    String myName;
    String myDescription;
    String myCode;
    String myManufacturer;

    /**
     * Constructor for this class.
     *
     * @return an instance of {@link ProductBuilder}.
     */
    public ProductBuilder() {}

    /**
     * Returns the name.
     *
     * @return the name
     */
    public String getName()
    {
        return myName;
    }

    /**
     * Returns the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return myDescription;
    }

    /**
     * Returns the code.
     *
     * @return the code
     */
    public String getCode()
    {
        return myCode;
    }

    /**
     * Returns the manufacturer.
     *
     * @return the manufacturer
     */
    public String getManufacturer()
    {
        return myManufacturer;
    }

    /**
     * Sets the name in this builder.
     *
     * @param pName the product name
     * @return an instance of this builder with this parameter set
     */
    public ProductBuilder withName(String pName)
    {
        this.myName = pName;
        return this;
    }

    /**
     * Sets the description in this builder.
     *
     * @param pDescription the description
     * @return an instance of this builder with this parameter set
     */
    public ProductBuilder withDescription(String pDescription)
    {
        this.myDescription = pDescription;
        return this;
    }

    /**
     * Sets the code in this builder.
     *
     * @param pCode the code
     * @return an instance of this builder with this parameter set
     */
    public ProductBuilder withCode(String pCode)
    {
        this.myCode = pCode;
        return this;
    }

    /**
     * Sets the manufacturer in this builder.
     *
     * @param pManufacturer the manufacturer
     * @return an instance of this builder with this parameter set
     */
    public ProductBuilder withManufacturer(String pManufacturer)
    {
        this.myManufacturer = pManufacturer;
        return this;
    }

    /**
     * Returns an instance of {@link Product} with automatically data.
     * @return an instance of {@link Product} with automatically data
     */
    public Product generateProduct(Faker pFaker)
    {
        this.myName = pFaker.name().fullName();
        this.myDescription = pFaker.address().streetAddress();
        this.myCode = pFaker.code().isbn13();
        this.myManufacturer = pFaker.company().name();

        return new Product(this);
    }

    /**
     * Returns a list of {@link Product} with automatically data.
     *
     * @return a list of {@link Product} with automatically data
     * @throws Exception
     */
    public List<Product> generateProducts(int pQuantity) throws Exception
    {
        validateQuantity(pQuantity);
        ArrayList<Product> products = new ArrayList<>(pQuantity);
        Faker faker = new Faker(new Locale("pt-BR"));

        IntStream.range(0, pQuantity).forEach(p -> { products.add(generateProduct(faker)); });

        return products;
    }

    /**
     * Validates the number of {@link Product} to be created.
     * 
     * @param pQuantity the quantity
     * @throws Exception
     */
    private void validateQuantity(int pQuantity) throws Exception
    {
        if (pQuantity < 0 || pQuantity > 100000)
        {
            throw new Exception("The quantity informed is invalid!<br>Quantity limits are from 1 to 100.000");
        }
    }

    /**
     * Creates a {@link Product} object properly set.
     *
     * @return a {@link Product} object
     */
    public Product build()
    {
        return new Product(this);
    }
}
